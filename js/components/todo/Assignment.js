export default {
    template: `
        <li>
            <label :class="{
                'in-progress': !assignment.complete,
                'completed': assignment.complete
                }"
            >
                <input type="checkbox" v-model="assignment.complete">
                {{assignment.name}}
            </label>
        </li>
    `,
    props: {
        assignment: Object
    }
}