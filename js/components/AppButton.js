export default {
    template: `
      <button
          :class="{
            'border rounded p-2 disabled:cursor-not-allowed': true,
            'bg-blue-700 hover:bg-blue-400': type === 'primary',
            'bg-purple-200 hover:bg-purple-400': type === 'secondary',
            'bg-gray-200 hover:bg-gray-400': type === 'muted',
            'is-loading': processing
          }"
          :disabled="processing"
          @click="startProcessing"
      >
        <slot/>
      </button>
    `,
    props: {
        type: {
            type: String,
            default: 'primary'
        },
        processing: {
            type: Boolean,
            default: false
        }
    },
    methods: {
        startProcessing() {
            this.processing = true;
            setTimeout(() => {
                this.processing = false;
            }, 3000);
        }
    }
}